FROM openjdk:latest

WORKDIR /usr/src/app

COPY OlaUnicamp.java OlaUnicamp.java ./

RUN javac OlaUnicamp.java

RUN java OlaUnicamp
