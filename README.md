# inf335-trabalho5_2

Trabalho da disciplina INF335 – Ambientes para Concepção de Software.

Atividade 5.2 – Jenkins + Docker

Projeto Hello Unicamp

```
docker build -t hello-unicamp .
```


## Observações na instalação do jenkins:
Foi utilizada a imagem jenkinsci/blueocean

O comando necessário para inicar o docker foi:
```
docker run -v /var/run/docker.sock:/var/run/docker.sock -p 8080:8080 -p 50000:50000 jenkinsci/blueocean
```

Foi necessário atualizar a permissão do docker.sock, com o comando:
```
sudo chmod 777 /var/run/docker.sock
```
